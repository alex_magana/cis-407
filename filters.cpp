#include "filters.h"
#include <stdio.h>

void Shrinker::Execute()
{
    int newWidth = img->getWidth()/2;
    int newHeight = img->getHeight()/2;
    image.setWidth(newWidth);
    image.setHeight(newHeight);
    image.setBuffer();
    
    for (int i = 0; i < image.getHeight(); i++) {
        for (int j = 0; j < image.getWidth(); j++) {
            int temp  = 3 * (i*image.getWidth() + j);
            int temp2 = 3 * (i * 2 * img->getWidth()+2*j);
            image.getBuffer()[temp] = img->getBuffer()[temp2];
            image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
            image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
        }
    }
}

void LRConcat::Execute()
{
    int newWidth = img->getWidth() + img2->getWidth();
    int height = 0;
    
    if (img->getHeight() > img2->getHeight()) {
        height = img->getHeight();
    }
    else
        height = img2->getHeight();
    
    image.setHeight(height);
    image.setWidth(newWidth);
    image.setBuffer();
    
    for (int i = 0; i < image.getHeight(); i++) {
        for (int j = 0; j < image.getWidth(); j++) {
            
            int temp = 3 * (i * image.getWidth() + j);
            
            if (j < img->getWidth())
            {
                int temp2 = 3 * (i * img->getWidth() + j);
                image.getBuffer()[temp] = img->getBuffer()[temp2];
                image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
                image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
            }
            else
            {
                int temp2 = 3 * (i * img2->getWidth() + j);
                image.getBuffer()[temp] = img2->getBuffer()[temp2];
                image.getBuffer()[temp+1] = img2->getBuffer()[temp2+1];
                image.getBuffer()[temp+2] = img2->getBuffer()[temp2+2];
            }
        }
    }
}

void TBConcat::Execute()
{
    int newHeight = img->getHeight() + img2->getHeight();
    int width = 0;
    if (img->getWidth() > img2->getWidth())
    {
        width = img->getWidth();
    }
    else
        width = img2->getWidth();
    
    image.setHeight(newHeight);
    image.setWidth(width);
    image.setBuffer();
    
    for (int j = 0 ; j < image.getWidth() ; j++)
        for (int i = 0 ; i < img->getHeight() ; i++)
        {
            int temp = i*image.getWidth() +j;
            int temp2 = i*image.getWidth() +j;
            image.getBuffer()[3*temp]   = img->getBuffer()[3*temp2];
            image.getBuffer()[3*temp+1] = img->getBuffer()[3*temp2+1];
            image.getBuffer()[3*temp+2] = img->getBuffer()[3*temp2+2];
        }
    
    for (int j = 0 ; j < image.getWidth() ; j++)
        for (int i = 0 ; i < img2->getHeight() ; i++)
        {
            int temp = (i+img->getHeight())*image.getWidth() +j;
            int temp2 = i*image.getWidth() +j;
            image.getBuffer()[3*temp]   = img2->getBuffer()[3*temp2];
            image.getBuffer()[3*temp+1] = img2->getBuffer()[3*temp2+1];
            image.getBuffer()[3*temp+2] = img2->getBuffer()[3*temp2+2];
        }
}

void Blender::Execute()
{
    int height = img->getHeight();
    int width = img->getWidth();
    
    image.setHeight(height);
    image.setWidth(width);
    image.setBuffer();
    
    for (int i = 0; i < image.getHeight(); i++) {
        for (int j = 0; j < image.getWidth(); j++) {
            int temp = 3 * (i * image.getWidth() + j);
            int temp2 = 3 * (i * image.getWidth() + j);
            image.getBuffer()[temp] = img->getBuffer()[temp2] * factor + img2->getBuffer()[temp2] * (1-factor);
            image.getBuffer()[temp+1] = img->getBuffer()[temp2+1] * factor + img2->getBuffer()[temp2+1] * (1-factor);
            image.getBuffer()[temp+2] = img->getBuffer()[temp2+2] * factor + img2->getBuffer()[temp2+2] * (1-factor);
        }
    }
}

void Blender::SetFactor(float x)
{
    this->factor = x;
}