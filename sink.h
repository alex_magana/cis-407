#ifndef _sink__
#define _sink__

#include "image.h"

class sink
{
protected:
    const Image *img;
    const Image *img2;
    
public:
    void SetInput(const Image *img);
    void SetInput2(const Image *img2);
    sink();
};

#endif
