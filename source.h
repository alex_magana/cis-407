#include "image.h"

#ifndef _source__
#define _source__

class source
{
protected:
    Image image;
    virtual void Execute() = 0;
    
public:
    Image* GetOutput();
    source();
    virtual void Update();
};


#endif
