#ifndef _PNMReader_h
#define _PNMReader_h

#include <string.h>
#include <stdio.h>
#include "source.h"

class PNMreaderCPP: public source
{
protected:
    char *file;
    
public:
    PNMreaderCPP(char *file);
    virtual void Execute();
    virtual void Update();
};


#endif
