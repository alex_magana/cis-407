#ifndef _filters_h
#define _filters_h

#include "sink.h"
#include "source.h"
#include <stdio.h>

class Filter: public source, public sink
{
public:
    virtual void Update();
};

void Filter::Update()
{
    if (img != NULL)
    {
        img->Update();
        Execute();
    }
    
    else if (img2 != NULL)
    {
        img2->Update();
        Execute();
    }
}

class filter: public source, public sink
{
    virtual void Update();
};

class Shrinker: public filter
{
public:
    void Execute();
};

class LRConcat: public filter
{
public:
    void Execute();
};

class TBConcat: public filter
{
public:
    void Execute();
};

class Blender: public filter
{
    
protected:
    float factor;
    
public:
    void SetFactor(float x);
    void Execute();
};

#endif
