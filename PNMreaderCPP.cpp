#include "PNMreader.h"
#include <fstream>

void PNMreaderCPP::Update()
{
    Execute();
}

PNMreaderCPP::PNMreaderCPP(char *file)
{
    this->file = file;
}

void PNMreaderCPP::Execute()
{
    ifstream infile;
    infile.open("3D_input.pnm");
    char magicNum[128];
    int  width, height, maxval;
    
    
    if (infile == NULL)
    {
        fprintf(stderr, "Unable to open file %s\n", this->file);
    }
    
    while (infile) {
        infile >> magicNum >> width >> height >> maxval;

    }

    if (strcmp(magicNum, "P6") != 0)
    {
        fprintf(stderr, "Unable to read from file %s, because it is not a PNM file of type P6\n", this->file);
    }
    
    image.setSize(width, height, 255);
    fread(image.getBuffer(), 3*sizeof(unsigned char), image.getHeight() * image.getWidth(), f);
    
    fclose(f);
}