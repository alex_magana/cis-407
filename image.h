#ifndef _image__
#define _image__


#include <stdio.h>
#include "source.h"

class source;

class Image
{
private:
    int height, width, maxVal;
    unsigned char *buffer;
    source *src;
    
public:
    void setHeight(int h);
    void setWidth(int w);
    void setBuffer();
    int getHeight() const;
    int getWidth() const;
    unsigned char * getBuffer() const;
    void setSize(int width, int height, int magicNum);
    
    void setSource(source *s);
    void Update() const;
    
    /* Constructors */
    Image();
    Image(int h, int w, unsigned char *b);
    Image(Image &img);
};

#endif
