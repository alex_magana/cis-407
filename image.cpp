#include "image.h"
#include "source.h"
#include <stdlib.h>

void Image::setHeight(int h)
{
    height = h;
}

void Image::setWidth(int w)
{
    width = w;
}

void Image::setBuffer()
{
    buffer = (unsigned char *) malloc(sizeof(unsigned char) * 3 * height * width);
}

int Image::getHeight() const
{
    return height;
}

int Image::getWidth() const
{
    return width;
}

unsigned char* Image::getBuffer() const
{
    return buffer;
}

void Image::setSize(int w, int h, int m)
{
    width  = w;
    height = h;
    maxVal = m;
    buffer = new unsigned char[3 * width * height];
}

void Image::setSource(source *s)
{
    src = s;
}

void Image::Update() const
{
    src->Update();
}

/* Constructors */

Image::Image()
{
    height = 0;
    width = 0;
    buffer = NULL;
}

Image::Image(int h, int w, unsigned char *b)
{
    height = h;
    width  = w;
    buffer = b;
}

Image::Image(Image &img)
{
    height = img.height;
    width  = img.width;
    buffer = img.buffer;
    
}