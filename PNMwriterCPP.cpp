#include "PNMwriter.h"
#include <string.h>
#include <fstream>

void PNMwriterCPP::Write(char *filename)
{
//    FILE *out_file = fopen(filename, "wb");
    ofstream out_file("3D_output.pnm");
    
    if (out_file == NULL) {
        fprintf(stderr, "Unable to open file %s\n", filename);
    }
    
    fprintf(out_file, "P6\n%d %d\n255\n", img->getWidth(), img->getHeight());
    fwrite(img->getBuffer(), sizeof(unsigned char), 3 * img->getWidth() * img->getHeight(), out_file);
    fclose(out_file);
}